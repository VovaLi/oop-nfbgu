class SquareFunction():
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def __call__(self, *args, **kwargs):
        return self.a*args[0]*args[0] + self.b*args[0]+self.c


sf = SquareFunction(0, 0, 1)
print(sf(-2))
print(sf(-1))
print(sf(-0))
print(sf(1))
print(sf(2))
print(sf(10))

