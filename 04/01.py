class FoodInfo():
    def __init__(self, p, f, c):
        self.pr = p
        self.fats = f
        self.carb = c

    def get_proteins(self):
        return self.pr

    def get_fats(self):
        return self.fats

    def get_carbohydrates(self):
        return self.carb

    def get_kcalories(self):
        return 4*self.pr+9*self.fats+4*self.carb

    def __add__(self, other):
        return FoodInfo(self.pr+other.pr, self.fats+other.fats, self.carb+other.carb)


food1 = FoodInfo(1, 2, 3)
food2 = FoodInfo(10, 20, 30)

food3 = food1 + food2
food4 = food2 + food1

print(food3.get_proteins(), food3.get_fats(),
      food3.get_carbohydrates(), food3.get_kcalories())
print(food4.get_proteins(), food4.get_fats(),
      food4.get_carbohydrates(), food4.get_kcalories())
