class ReversedList():
    def __init__(self, m):
        self.l = list()
        for i in m:
            self.l.insert(0, i)

    def __len__(self):
        return len(self.l)

    def __getitem__(self, item):
        return self.l[item]


rl = ReversedList([10])
print(len(rl))
print(rl[0])
    
