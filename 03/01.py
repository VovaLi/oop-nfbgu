class LeftParagraph:
    def __init__(self, l):
        self.lengthParagraph = l
        self.arr = list()
        self.arr.append('')

    def add_word(self, word):
        if len(self.arr[len(self.arr)-1]) + len(word) + 1 > self.lengthParagraph:
            self.arr.append(word)
            return
        if self.arr[len(self.arr)-1] == '':
            self.arr[len(self.arr)-1] = self.arr[len(self.arr)-1] + word
            return
        self.arr[len(self.arr)-1] = self.arr[len(self.arr)-1] + ' ' + word

    def end(self):
        for i in self.arr:
            print(i)


class RightParagraph:
    def __init__(self, l):
        self.lengthParagraph = l
        self.arr = list()
        self.arr.append('')

    def add_word(self, word):
        if len(self.arr[len(self.arr) - 1]) + len(word) + 1 > self.lengthParagraph:
            self.arr.append(word)
            return
        if self.arr[len(self.arr) - 1] == '':
            self.arr[len(self.arr) - 1] = self.arr[len(self.arr) - 1] + word
            return
        self.arr[len(self.arr) - 1] = self.arr[len(self.arr) - 1] + ' ' + word

    def end(self):
        for i in self.arr:
            print(i.rjust(self.lengthParagraph, ' '))


lp = LeftParagraph(8)
lp.add_word('abc')
lp.add_word('defg')
lp.add_word('hi')
lp.add_word('jklmnopq')
lp.add_word('r')
lp.add_word('stuv')
lp.end()
print()

rp = RightParagraph(8)
rp.add_word('abc')
rp.add_word('defg')
rp.add_word('hi')
rp.add_word('jklmnopq')
rp.add_word('r')
rp.add_word('stuv')
rp.end()
print()
