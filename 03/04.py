class Table:
    def __init__(self, row, column):
        self.r = row
        self.c = column
        self.arr = []
        for k in range(self.r):
            self.arr.append([0]*self.c)

    def get_value(self, row, col):
        if (row > self.r-1) or (col > self.c-1) or (row < 0) or (col < 0):
            return None
        return self.arr[row][col]

    def set_value(self, row, col, value):
        self.arr[row][col] = value

    def n_rows(self):
        return self.r

    def n_cols(self):
        return self.c


tab = Table(1, 1)

for i in range(tab.n_rows()):
    for j in range(tab.n_cols()):
        print(tab.get_value(i, j), end=' ')
    print()
print()

tab.set_value(0, 0, 1000)

for i in range(tab.n_rows()):
    for j in range(tab.n_cols()):
        print(tab.get_value(i, j), end=' ')
    print()
print()

for i in range(-1, tab.n_rows() + 1):
    for j in range(-1, tab.n_cols() + 1):
        print(tab.get_value(i, j), end=' ')
    print()
print()
