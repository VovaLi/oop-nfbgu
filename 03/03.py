import sys


class MinStat:
    def __init__(self):
        self.min = sys.maxsize
        self.val = list()

    def add_number(self, t):
        self.val.append(t)

    def result(self):
        if len(self.val) == 0:
            return None
        for i in self.val:
            if i < self.min:
                self.min = i
        return self.min


class MaxStat:
    def __init__(self):
        self.max = -sys.maxsize
        self.val = list()

    def add_number(self, t):
        self.val.append(t)

    def result(self):
        if len(self.val) == 0:
            return None
        for i in self.val:
            if i > self.max:
                self.max = i
        return self.max


class AverageStat:
    def __init__(self):
        self.avg = 0
        self.val = list()

    def add_number(self, t):
        self.val.append(t)

    def result(self):
        if len(self.val) == 0:
            return None
        for i in self.val:
            self.avg += i
        self.avg = self.avg / len(self.val)
        return self.avg


values = [1, 0, 0]

mins = MinStat()
maxs = MaxStat()
average = AverageStat()
for v in values:
    mins.add_number(v)
    maxs.add_number(v)
    average.add_number(v)

print(mins.result(), maxs.result(), '{:<05.3}'.format(average.result()))
