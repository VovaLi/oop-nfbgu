class AmericanDate:
    def __init__(self, year, month, day):
        self.y = str(year).rjust(2, "0")
        self.m = str(month).rjust(2, "0")
        self.d = str(day).rjust(2, "0")

    def set_year(self, year):
        self.y = str(year)

    def set_month(self, month):
        self.m = str(month).rjust(2, "0")

    def set_day(self, day):
        self.d = str(day).rjust(2, "0")

    def get_year(self, year):
        print(self.y)

    def get_month(self, month):
        print(self.m)

    def get_day(self):
        print(self.d)

    def format(self):
        return str(self.m+'.'+self.d+'.'+self.y)


class EuropeanDate:
    def __init__(self, year, month, day):
        self.y = str(year).rjust(2, "0")
        self.m = str(month).rjust(2, "0")
        self.d = str(day).rjust(2, "0")

    def set_year(self, year):
        self.y = str(year)

    def set_month(self, month):
        self.m = str(month).rjust(2, "0")

    def set_day(self, day):
        self.d = str(day).rjust(2, "0")

    def get_year(self, year):
        print(self.y)

    def get_month(self, month):
        print(self.m)

    def get_day(self):
        print(self.d)

    def format(self):
        return str(self.d+'.'+self.m+'.'+self.y)


american = AmericanDate(2000, 4, 10)
european = EuropeanDate(2000, 4, 10)
print(american.format())
print(european.format())

