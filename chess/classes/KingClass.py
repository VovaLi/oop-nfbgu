from interfaces.FigureInterface import Figure
from functions import COLOR_WHITE


class King(Figure) :
    def char(self):
        if self.get_color() == COLOR_WHITE:
            return '\u2654'
        return '\u265A'

    def can_move(self, board, from_row, from_col, to_row, to_col):
        if abs(from_row - to_row) != abs(from_col - to_col) and from_row != to_row and from_col != to_col:
            return False
        step_row = 1 if (to_row >= from_row) else -1
        step_col = 1 if (to_col >= from_col) else -1
        from_row += step_row
        from_col += step_col
        return True
