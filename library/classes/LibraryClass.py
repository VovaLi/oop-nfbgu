from classes.BookClass import Book
class Library(object):
    def __init__(self):
        self.bookList = list()

    def open(self):
        f = open('libr.txt', encoding='utf-8')
        for line in f:
            self.bookList.append(Book(line.split(".")[0], line.split(".")[1], line.split(".")[2]))

    def addBook(self):
        n = input("Введите название:")
        a = input("Введите автора:")
        y = input("Введите год:")
        self.bookList.append(Book(n, a, y))
        self.close()

    def changeBook(self):
        s = self.findBook(input("Введите id:"))
        if s == 0:
            return
        if not s.isAvailable:
            return "Извините книгу уже забрали"

        n = input("Введите новое название:")
        a = input("Введите нового автора:")
        y = input("Введите новый год:")

        s.changeBook(s.num, n, a, y)
        self.close()

    def takeBook(self):
        book = self.findBook(input("Введите id:"))
        if book == 0:
            return
        if not book.isAvailable:
            return "Извините книгу уже забрали"
        if book.isAvailable:
            book.isAvailable = False
            print("Взяли книгу: ", book)

    def returnBook(self):
        book = self.findBook(input("Введите id: "))
        if book == 0:
            return
        if not book.isAvailable:
            book.isAvailable = True
            print("Вернули книгу: ", book)

    def deleteBook(self):
        x = -1
        i = self.findBook(input("Введите id: "))
        if i == 0:
            return
        if not i.isAvailable:
            return "Книгу забрали"
        self.bookList.remove(i)
        i.__del__()
        Book.bookNum = 1
        for z in self.bookList:
            z.changeBook(Book.bookNum)
            Book.bookNum += 1
        self.close()

    def getAllBooks(self):
        if len(self.bookList) == 0:
            return "Книг нет"
        for i in self.bookList:
            print(i)

    def printBook(self):
        k = self.findBooks(input("Введите название: "))
        if k != None:
            for i in k:
                print(i)

    def findBook(self, f):
        for i in self.bookList:
            if str(i.num) == str(f):
                return i
        print("Такой книги нет в нашем списке")
        return 0

    def findBooks(self, f):
        listReturn = list()
        for i in self.bookList:
            if f.upper().strip() in i.name.upper():
                listReturn.append(i)
        if len(listReturn) != 0:
            return listReturn

    def close(self):
        f = open('libr.txt', 'w', encoding='utf-8')
        for i in self.bookList:
            f.write(i.name+"."+i.author+"."+i.year+"\n")
