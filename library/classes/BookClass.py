class Book(object):
    bookNum = 1

    def __init__(self, name, author, year):
        self.isAvailable = True
        self.num = Book.bookNum
        Book.bookNum += 1
        self.name = name.strip()
        self.author = author.strip()
        self.year = year.strip()

    def changeBook(self, newNum="", newName="", newAuthor="", newYear=""):
        if not self.isAvailable:
            return "Книгу забрали"
        if newNum != "":
            self.num = int(str(newNum).strip())
        if newName != "":
            self.name = newName.strip()
        if newAuthor != "":
            self.author = newAuthor.strip()
        if newYear != "":
            self.year = newYear.strip()

    def __del__(self):
        self.isAvailable = False
        self.name = ""
        self.year = 0
        self.author = ""
        self.num = 0

    def __str__(self):
        vzyali = ""
        if not self.isAvailable:
            vzyali = " - Взяли"
        return str(self.num)+" "+self.name+" "+self.author+" "+str(self.year) + vzyali
