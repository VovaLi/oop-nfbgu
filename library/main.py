from classes.LibraryClass import Library

myLibrary = Library()
ex = False

while not ex:
    print('\nВыберите действие\n\n' \
             f'1) Удалить книгу (id)\n' \
             f'2) Найти книгу (id)\n' \
             f'3) Добавить книгу\n' \
             f'4) Изменить книгу\n' \
             f'5) Вывести все книги\n' \
             f'6) Взять книгу\n' \
             f'7) Вернуть книгу\n' \
             f'8) Выход\n')

    t = input()
    if t == "1":
        myLibrary.deleteBook()
    elif t == "2":
        myLibrary.printBook()
    elif t == "3":
        myLibrary.addBook()
    elif t == "4":
        myLibrary.changeBook()
    elif t == "5":
        myLibrary.getAllBooks()
    elif t == "6":
        myLibrary.takeBook()
    elif t == "7":
        myLibrary.returnBook()
    elif t == "8":
        ex = True
    else:
        print('Такого варианта действия не существует!\n')